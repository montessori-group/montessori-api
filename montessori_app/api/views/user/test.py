from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
# from rest_framework.views import APIView
from montessori_app.api.mixins import APIView
from rest_framework import generics

from montessori_app.api.serializers.user.test import TestSerializer
from montessori_app.api.serializers.user.test import UserSerializer
from montessori_app.api.serializers.user.test import DxSerializer
from montessori_app.api.serializers.user.test import GroupResultSerializer
from montessori_app.api.serializers.user.test import ResultSerializer

from django.contrib.auth.models import BaseUserManager
from montessori_app.users.models import User
from montessori_app.tests.models import DxTest
from montessori_app.tests.models import Dx
from montessori_app.tests.models import Test
from montessori_app.tests.models import GroupResult
from montessori_app.tests.models import Result
from montessori_app.tests.models import OptionValue
from montessori_app.tests.models import Question


class StartTest(APIView):
    serializer_class = GroupResultSerializer

    def post(self, request, *args, **kwargs):

        try:
            group_result = GroupResult.objects.get(dx=kwargs['id'], user=self.get_user().id, is_active=True)
            """Close the GroupResult test and create a GroupResult with the next test if it exist"""
            group_result.is_active = False
            group_result.save()

            test = DxTest.objects.get(dx=group_result.dx, test=group_result.test)
            try:
                dx_test = DxTest.objects.get(dx=group_result.dx, position=test.position + 1)
                response = self.create_group_result(dx_test.test)
                response['data']['end_dx'] = False
                return Response(response['data'], status=response['status'])
            except DxTest.DoesNotExist:
                return Response({"end_dx": True}, status=status.HTTP_200_OK)

        except GroupResult.DoesNotExist:
            """Create a new GroupResult with the first Dx Test"""
            dx_test = DxTest.objects.get(position=1, dx=kwargs['id'])
            response = self.create_group_result(dx_test.test)
            response['data']['end_dx'] = False
            return Response(response['data'], status=response['status'])

    def create_group_result(self, test):
        data = {'user': self.get_user().id, 'dx': self.kwargs['id'], 'test': test.id}
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            return {"data": serializer.data, "status": status.HTTP_200_OK}
        return {"data": serializer.errors, "status": status.HTTP_400_BAD_REQUEST}


class UserView(BaseUserManager, generics.GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        user, created = User.objects.get_or_create(username=self.username().upper())
        serializer = self.serializer_class(user)
        if created:
            user.first_name = request.data['first_name']
            user.last_name = request.data['last_name']
            user.age = request.data['age']
            user.set_password(request.data['password'])
            user.save()
        return Response({"user": serializer.data, "created": created}, status=status.HTTP_200_OK)

    def username(self):
        return '{}{}'.format(self.request.data['first_name'][0] or '',
                             self.request.data['last_name'].split(' ')[0] or '').strip()


class Dx(APIView, generics.ListAPIView):
    serializer_class = DxSerializer
    queryset = Dx.objects.all()


class TestView(APIView, generics.RetrieveAPIView):
    serializer_class = TestSerializer
    queryset = Test.objects.all()

    def get_object(self):
        group_result = get_object_or_404(GroupResult, user=self.get_user().id, is_active=True)
        obj = get_object_or_404(self.get_queryset(), pk=group_result.test.pk)
        return obj

    def get_serializer_context(self):
        group_result = get_object_or_404(GroupResult, user=self.get_user().id, is_active=True)
        return {'group_id': group_result}


class AnswerQuestion(APIView, generics.CreateAPIView):
    serializer_class = ResultSerializer

    def create(self, request, *args, **kwargs):
        group_result = GroupResult.objects.get(is_active=True, user=self.get_user().id)
        question = Question.objects.get(pk=request.data['question'])
        option_value = OptionValue.objects.get(pk=request.data['option_value'])

        obj, created = Result.objects.update_or_create(question=question,
                                                       group_result=group_result,
                                                       defaults={'option_value': option_value})
        response = self.serializer_class(obj)
        return Response(response.data, status=status.HTTP_200_OK)
