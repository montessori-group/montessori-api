from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status

from montessori_app.tests.models import GroupResult

from montessori_app.api.serializers.user.test import UserSerializer
from montessori_app.api.serializers.user.test import TestSerializer
from montessori_app.api.serializers.user.test import Test


class Login(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        # Check if user has a test in progress
        try:
            group_result = GroupResult.objects.get(user=user, is_active=True)
            # test = TestSerializer(Test.objects.get(pk=group_result.test_id), context={'group_id':
            # group_result.pk}).data
            dx = group_result.dx.pk
        except GroupResult.DoesNotExist:
            dx = None

        response = {
            'token': token.key,
            'user': UserSerializer(user).data,
            'dx': dx
        }

        return Response(response, status=status.HTTP_200_OK)
