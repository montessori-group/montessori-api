from django.urls import path, include
from montessori_app.api.views.user import test
from montessori_app.api.views.user import user
from montessori_app.api.views.user import email

router_config = {'get': 'retrieve', 'post': 'create', 'put': 'update', 'delete': 'destroy'}

dx = [
    path('',
         test.Dx.as_view(),
         name='dx'),
]

test_url = [
    path('',
         test.TestView.as_view(),
         name='test'),
    path('dx/<str:id>/',
         test.StartTest.as_view(),
         name='test'),
    path('answer/',
         test.AnswerQuestion.as_view(),
         name='test')
]

user = [
    path('sing-up/',
         test.UserView.as_view(),
         name='sing-up'),
    path('login/',
         user.Login.as_view(),
         name='login'),
]

email = [
    path('',
         email.SendEmail.as_view(),
         name='email')
]

urlpatterns = [
    path('dx/', include(dx)),
    path('test/', include(test_url)),
    path('user/', include(user)),
    path('email/', include(email))
]
