from django.contrib import admin
from .models import Dx, Test, Scale, DxTest, Range, ScaleRange, Dimension, Question, \
    Option, OptionValue, Result, GroupResult


@admin.register(Dx, Test, Scale, DxTest, Range, ScaleRange, Dimension, Question, Option, OptionValue, Result, GroupResult)
class Test(admin.ModelAdmin):
    pass
