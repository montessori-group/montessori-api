from django.db import models
from montessori_app.users.models import User


class Dx(models.Model):
    dx = models.TextField()
    not_problem = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class Test(models.Model):
    name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    published_year = models.CharField(max_length=4)
    description = models.TextField()
    recommendation = models.TextField()
    not_problem = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class Scale(models.Model):
    scale = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class DxTest(models.Model):
    percent = models.DecimalField(max_digits=4, decimal_places=2)
    dx = models.ForeignKey(Dx,
                           related_name='dx_scale',
                           on_delete=models.CASCADE)
    test = models.ForeignKey(Test,
                             related_name='dx_scale',
                             on_delete=models.CASCADE)
    position = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class Range(models.Model):
    min_range = models.DecimalField(max_digits=9, decimal_places=2)
    max_range = models.DecimalField(max_digits=9, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class ScaleRange(models.Model):
    scale = models.ForeignKey(Scale,
                              related_name='scale_range',
                              on_delete=models.CASCADE)
    range = models.ForeignKey(Range,
                              related_name='scale_range',
                              on_delete=models.CASCADE)


class Dimension(models.Model):
    dimension = models.TextField()
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class Question(models.Model):
    question = models.TextField()
    test = models.ForeignKey(Test,
                             related_name='questions',
                             on_delete=models.CASCADE)
    dimension = models.ForeignKey(Dimension,
                                  related_name='questions',
                                  null=True,
                                  blank=True,
                                  on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class Option(models.Model):
    option = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class OptionValue(models.Model):
    value = models.DecimalField(max_digits=9, decimal_places=3)
    question = models.ForeignKey(Question,
                                 related_name='options_value',
            
                                 on_delete=models.CASCADE)
    option = models.ForeignKey(Option,
                               related_name='options_value',
                               on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now_add=True, editable=False, blank=True)


class GroupResult(models.Model):
    user = models.ForeignKey(User,
                             related_name='group_result',
                             on_delete=models.CASCADE)
    test = models.ForeignKey(Test,
                             related_name='group_result',
                             on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    dx = models.ForeignKey(Dx,
                           related_name='group_result',
                           on_delete=models.CASCADE)


class Result(models.Model):
    question = models.ForeignKey(Question,
                                 related_name='result',
                                 on_delete=models.CASCADE)
    option_value = models.ForeignKey(OptionValue,
                                     related_name='result',
                                     on_delete=models.CASCADE)
    group_result = models.ForeignKey(GroupResult,
                                     related_name='results',
                                     on_delete=models.CASCADE)
