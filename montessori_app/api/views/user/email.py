from rest_framework.response import Response
from rest_framework import status
from django.core.mail import BadHeaderError, send_mail
from rest_framework.views import APIView


class SendEmail(APIView):

    def post(self, request, *args, **kwargs):

        message = request.data['message']
        name = request.data['name']
        from_email = request.data['email']
        subject = name + ' escribió por mi pagina web.'

        if subject and message and from_email:
            try:
                send_mail(subject, from_email + ' - ' + message, from_email, ['rivas28.2.96@gmail.com'])
            except BadHeaderError:
                return Response({"detail": "No se pudo enviar el correo, por favor intente nuevamente."},
                                status=status.HTTP_400_BAD_REQUEST)
            return Response({"detail": "Se envió el mensaje con éxito, gracias por comunicarte conmigo."},
                            status=status.HTTP_200_OK)
        else:
            # In reality we'd use a form class
            # to get proper validation errors.
            return Response({"detail": "No se recibieron todos los campos obligatorios."},
                            status=status.HTTP_400_BAD_REQUEST)
