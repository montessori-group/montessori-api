from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from import_export.admin import ImportExportModelAdmin

from montessori_app.users.models import User


@admin.register(User)
class UserAdmin(ImportExportModelAdmin, UserAdmin):
    search_fields = ('first_name', 'last_name', 'username')
    fieldsets = (
        (None, {'fields': ('username', 'password',)}),
        ('Personal info', {
            'fields': ('first_name', 'last_name', 'role')
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_admin', 'is_superuser', 'groups', 'user_permissions'),
        }),
        ('Important dates', {
            'fields': ('last_login',)
        }),
    )
    list_display = ('username', 'first_name', 'last_name', 'is_active',)
    list_filter = ('is_superuser', 'is_active', 'groups',)
