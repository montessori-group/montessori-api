from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.validators import validate_email

from montessori_app.users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    CUSTOMER_ROLE = 'CUSTOMER'
    ADMIN_ROLE = 'ADMIN'
    ROLES_CHOICES = [
        (CUSTOMER_ROLE, 'customer'),
        (ADMIN_ROLE, 'admin')
    ]

    USERNAME_FIELD = 'username'
    objects = UserManager()

    username = models.CharField(max_length=64, unique=True)

    first_name = models.CharField(max_length=64, null=True, blank=True)
    last_name = models.CharField(max_length=64, null=True, blank=True)
    role = models.CharField(max_length=10, choices=ROLES_CHOICES, default=CUSTOMER_ROLE)
    age = models.CharField(max_length=3, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now=True, editable=False, blank=True)

    def __str__(self):
        return '{}'.format(self.username)

    @staticmethod
    def generate_pass(first_name, last_name):
        generated_pass = str(first_name)[0] + str(last_name)
        return generated_pass

    def check_generated_pass(self):
        generated_pass = self.generate_pass(self.first_name, self.last_name)
        if self.check_password(generated_pass):
            return generated_pass
        else:
            return 'this password was changed'

    @property
    def username_is_email(self):
        try:
            validate_email(self.username)
            return True
        except validate_email.ValidationError:
            return False

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name or '', self.last_name or '').strip()

    @property
    def short_name(self):
        short_name = ''
        if self.first_name and len(self.first_name):
            short_name += self.first_name.strip()[0]
        if self.last_name and len(self.last_name):
            short_name += '. ' + self.last_name.strip().split(' ')[0]
        return short_name
