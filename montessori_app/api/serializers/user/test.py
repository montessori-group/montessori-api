from rest_framework import serializers
from montessori_app.users.models import User
from montessori_app.tests.models import Test
from montessori_app.tests.models import Question
from montessori_app.tests.models import OptionValue
from montessori_app.tests.models import Result
from montessori_app.tests.models import GroupResult
from montessori_app.tests.models import Dx


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'age', 'role']


class OptionValueSerializer(serializers.ModelSerializer):
    option = serializers.SlugRelatedField(read_only=True, slug_field='option')
    selected = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = OptionValue
        fields = ['id', 'value', 'option', 'selected']

    def get_selected(self, obj):
        group_id = self.context.get('group_id', None)
        return Result.objects.filter(group_result=group_id, option_value=obj.id).exists()


class QuestionSerializer(serializers.ModelSerializer):
    options_value = OptionValueSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ['id', 'question', 'options_value']


class TestSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Test
        fields = ['name', 'author', 'published_year', 'description', 'recommendation', 'questions']


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ['question', 'option_value', 'group_result']


class GroupResultSerializer(serializers.ModelSerializer):
    results = ResultSerializer(many=True, read_only=True)

    class Meta:
        model = GroupResult
        fields = ['results']


class DxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dx
        fields = ['id', 'dx', 'not_problem']


class GroupResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupResult
        fields = ['user', 'test', 'is_active', 'dx']
