from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):

    def create_user(self, username, password, first_name='', last_name='', role=''):
        if not username:
            raise ValueError('User must have an username address')
        user = self.model(
            username=self.model.normalize_username(username))
        user.first_name = first_name
        user.last_name = last_name
        user.role = role
        if not password:
            password = self.model.generate_pass(first_name, last_name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username, password=password)
        user.is_admin = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
