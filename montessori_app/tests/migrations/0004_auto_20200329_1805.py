# Generated by Django 2.2.10 on 2020-03-29 23:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0003_auto_20200329_1801'),
    ]

    operations = [
        migrations.AlterField(
            model_name='optionvalue',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='options_value', to='tests.Question'),
        ),
    ]
